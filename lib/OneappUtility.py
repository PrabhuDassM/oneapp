import ConfigParser
import os
import re

def readINI(filePath,fileName):
	Config = ConfigParser.ConfigParser()
	Config.read(str(filePath)+'\\'+str(fileName))
	return Config
	
def convertPDF(libraryFilePath,pdfName,textFileName):
	os.system(libraryFilePath+'\\pdftotext.exe -raw '+pdfName+' '+textFileName)
	file = open(textFileName, 'r') 
	# need to add exception control for file to kill
	content = file.read()
	return content
	
def formatIdentify(pdfFilecontent):
	pdfFormat=''
	if re.search('1\.\s*Applicant\s*Name\s*\,\s*Address\s*and\s*Contact\s*Details',pdfFilecontent,re.I):
		pdfFormat="Format1"
		pdfFormatTmp = 'F1'
		pdfFilecontent = textReconstruct(pdfFilecontent,pdfFormat)
		
	elif re.search('1\s*\.\s*Site\s*Address|1\s*\.\s*Site\s*Details',pdfFilecontent,re.I):
		pdfFormat="Format2"
		pdfFormatTmp = 'F2'
		pdfFilecontent = textReconstruct(pdfFilecontent,pdfFormat)

	else:
		pdfFormat="Others"
		pdfFormatTmp = 'O'

	return (pdfFormat, pdfFilecontent, pdfFormatTmp)

def textReconstruct(pdfFilecontent,Format):
	macthedListTmp=pdfFilecontent.split('\n')
	reConstructed = ''
	(Prev_Title_Number,greatestNumber) = (0,0)
	for line in macthedListTmp:
		line = re.sub(r'\s*', '', line)
		if re.search('[\w]+',line) is not None:
			Title_Number=re.match('^(\d+)\.\s*[a-z]+',line,re.I)
			if Title_Number:
				Next_Title_Number = int(Title_Number.group(1))
				Prev_Title_Number = Prev_Title_Number+1
				if Next_Title_Number >= Prev_Title_Number:
					Prev_Title_Number = Next_Title_Number
					if greatestNumber < Next_Title_Number:
						reConstructed = reConstructed+"|Split|"+"\n"+line
						greatestNumber=Prev_Title_Number
				else:
					reConstructed = reConstructed+"\n"+line
					Prev_Title_Number = Next_Title_Number
			else:
				line = line+'<>\n'
				reConstructed=reConstructed+" "+line
	reConstructed = reConstructed+"|Split|"
	reConstructed = re.sub(r'(?:\n)?[\d]+\.\s*\(?[a-z\-\:\s]+\s*\(?continued\)', '', reConstructed) # to remove page continuation mark
	reConstructed = re.sub(r'Ref\s*\:\s*[^>]*?Planning\s*Portal\s*Reference\s*\:[^>]*?<>', '', reConstructed) # to remove page reference
	reConstructed = re.sub(r'Planning\s*Portal\s*Reference\s*\:\s*[^>]*?<>', '<>', reConstructed) # to remove page reference
	
	if re.search('A\s+p\s+p\s+l\s+i\s+c\s+a\s+n\s+t\s+N\s+a\s+m\s+e',reConstructed) is not None: # to remove unwanted space between each letters Eg: "A p p l i c a n t"
		reConstructed = re.sub(r'\s\s+', 'DoubleSpace', reConstructed)
		reConstructed = re.sub(r'\s+', '', reConstructed)
		reConstructed = re.sub(r'DoubleSpace', ' ', reConstructed)
	reConstructed = re.sub(r'<>(?:<>)+', '<>', reConstructed)
	return reConstructed
	
def commonClean(Value):
	Value = re.sub(r'\s*<>\s*', ' ', Value)	
	Value = re.sub(r'\s\s+', ' ', Value)	
	Value = re.sub(r'\|+', '|', Value)	
	Value = re.sub(r'^\||\|$', '', Value)	
	Value = re.sub(r'^\,|\,$', '', Value)		
	Value = re.sub(r'^\s+|\s+$', '', Value)
	Value = re.sub(r'\'', '\'\'', Value)
	Value = re.sub(r'^\|', '', Value)
	return Value
	
def cleanScrapedValue(content,regexFile,Format,regex):
	regex=regexFile.get(Format, regex)
	if regex:
		content = re.sub(regex, '', content)	
	return content
	
def scrapeValue(content,regexFile,Format,regex1,regex2,regex3,regex4,regex5):
	regex1=regexFile.get(Format, regex1)
	Value=re.findall(regex1, content, re.I)
	if Value:
		Value = Value[0]
	
	
	# print "content::",content
	# print "regexFile::",regex1
	# print "Vlaue1::",Value
	# raw_input("Value")
	if not Value and regex2 != '':
		# print "*****"
		regex2 = regexFile.get(Format, regex2)
		Value = re.findall(regex2, content, re.I)
		if Value:
			Value = Value[0]
		
	if not Value and regex3 != '':
		regex3=regexFile.get(Format, regex3)
		Value=re.findall(regex3, content, re.I)
		if Value:
			Value = Value[0]
		
	if not Value and regex4 != '':
		regex4=regexFile.get(Format, regex4)
		Value=re.findall(regex4, content, re.I)
		if Value:
			Value = Value[0]
		
	if not Value and regex5 != '':
		regex5=regexFile.get(Format, regex5)
		Value=re.findall(regex5, content, re.I)
		if Value:
			Value = Value[0]

	if Value:
		Value = commonClean(Value)
	else:			
		Value = ''
	# print "Vlaue1::",Value
	# raw_input("Value")	
	return Value
	


def scrapeMultiValue_temp(content,regexFile,Format,regex1,regex2,regex3,regex4,regex5):
	regex1=regexFile.get(Format, regex1)
	Value=re.findall(regex1, content, re.I)

	if not Value and regex2 != '':
		regex2 = regexFile.get(Format, regex2)
		Value = re.findall(regex2, content, re.I)
		
	if not Value and regex3 != '':
		regex3=regexFile.get(Format, regex3)
		Value=re.findall(regex3, content, re.I)
		
	if not Value and regex4 != '':
		regex4=regexFile.get(Format, regex4)
		Value=re.findall(regex4, content, re.I)
		
	if not Value and regex5 != '':
		regex5=regexFile.get(Format, regex5)
		Value=re.findall(regex5, content, re.I)

	if Value:
		if Value[0]:
			Value1 = commonClean(Value[0])
		if Value[1]:
			Value2 = commonClean(Value[1])
		if Value[2]:
			Value3 = commonClean(Value[2])
		if Value[3]:
			Value4 = commonClean(Value[3])
		if Value[4]:
			Value5 = commonClean(Value[4])
	else:			
		Value1 = ''
		Value2 = ''
		Value3 = ''
		Value4 = ''
		Value5 = ''								
	return (Value1,Value2,Value3,Value4,Value5)
	
	
def scrapeMultiValue(content,regexFile,Format,siteAreaRegexNameList):
	valueList=[]
	
	for eachName in siteAreaRegexNameList:
		# print ("Format::",Format)
		# print ("eachName::",eachName)
		if eachName !="":
			regex=regexFile.get(Format, eachName)
			# print ("regex::",regex)
			val = re.findall(regex, content, re.I)
			if val:
				valueList.append(val)
			if valueList:
				# print "valueList:",valueList
				break
	# print "val:",val				
	# print "valueList:",valueList
	# raw_input("SSS")
	try:	
		return valueList[0][0]
	except:
		return ""
	
	
def scrapeMaterials(materialsBlock, materialType, regexFile, Format):
		
	materialDetails = re.search(r'<M>\s*'+str(materialType)+'[^>]*?<>\s*([\w\W]*?)(?:<M>|Are\s*you\s*supplying)', materialsBlock, re.I)
	
	# materialDetails = re.findall(r'<M>\s*'+str(materialType)+'[^>]*?<>\s*([\w\W]*?)(?:<M>|Are\s*you\s*supplying)', materialsBlock, re.I) 
	
	materialDetailss = re.findall(r'<M>\s*'+str(materialType)+'[^>]*?<>\s*([\w\W]*?)(?:<M>|Are\s*you\s*supplying)', materialsBlock, re.I) 
	
	(existingMaterial, proposedMaterial,existingMaterials,proposedMaterials) = ('', '','','')
	# if materialDetails:
	for materialDetails in materialDetailss:
		# materialDetails = materialDetails.group(1)
		# materialDetails = eachKeyword.group(1)
		
		with open("materialDetails.txt","w") as f:
			f.write(str(materialDetails))
		
		materialDetails = re.sub(r'(>)\s*('+str(materialType)+'[^>]*?description\s*\:?)',r'', materialDetails, re.IGNORECASE)
		if (Format == "Format1"):
			materialDetails = re.sub(r'<>(\s*[\d]\s*\.\s*\(Materials\s*continued\))',r'<>', materialDetails, re.IGNORECASE)
		
		# print Format
		# print materialDetails
		# raw_input("pppppp")	
		
		existingMaterialList = re.findall(regexFile.get(Format, 'existingMaterials1'), materialDetails, re.I)
		if not existingMaterialList:
			existingMaterialList = re.findall(regexFile.get(Format, 'existingMaterials2'), materialDetails, re.I)
		if not existingMaterialList:
			existingMaterialList = re.findall(regexFile.get(Format, 'existingMaterials3'), materialDetails, re.I)
		if not existingMaterialList:
			existingMaterialList = re.findall(regexFile.get(Format, 'existingMaterials4'), materialDetails, re.I)
		if not existingMaterialList:
			existingMaterialList = re.findall(regexFile.get(Format, 'existingMaterials5'), materialDetails, re.I)
		if not existingMaterialList:
			existingMaterialList = re.findall(regexFile.get(Format, 'existingMaterials6'), materialDetails, re.I)
		if not existingMaterialList:
				existingMaterialList = re.findall(regexFile.get(Format, 'existingMaterials7'), materialDetails, re.I)
		
		proposedMaterialList = re.findall(regexFile.get(Format, 'proposedMaterials1'), materialDetails, re.I)
		if not proposedMaterialList:
			proposedMaterialList = re.findall(regexFile.get(Format, 'proposedMaterials2'), materialDetails, re.I)
		if not proposedMaterialList:
			proposedMaterialList = re.findall(regexFile.get(Format, 'proposedMaterials3'), materialDetails, re.I)
		if not proposedMaterialList:
			proposedMaterialList = re.findall(regexFile.get(Format, 'proposedMaterials4'), materialDetails, re.I)
		if existingMaterialList:	
			existingMaterial = '|'.join(existingMaterialList)
		if proposedMaterialList:	
			proposedMaterial = '|'.join(proposedMaterialList)
		existingMaterial=re.sub(r'<>',' ',existingMaterial)
		existingMaterial=commonClean(existingMaterial)
		proposedMaterial=re.sub(r'<>',' ',proposedMaterial)
		proposedMaterial=commonClean(proposedMaterial)
		
		existingMaterials=existingMaterials+"|"+existingMaterial
		proposedMaterials=proposedMaterials+"|"+proposedMaterial
		
		existingMaterials=commonClean(existingMaterials)
		proposedMaterials=commonClean(proposedMaterials)
	return (existingMaterials, proposedMaterials, materialDetails)

def checkMaterialOthers(existing,proposed,existingotherMaterial, proposedotherMaterial):
	existing+='|'+existingotherMaterial
	proposed+='|'+proposedotherMaterial
	(existingotherMaterial, proposedotherMaterial)=('', '')
	existing=commonClean(existing)
	proposed=commonClean(proposed)
	return (existing,proposed,existingotherMaterial, proposedotherMaterial)
	

def addOtherMaterialType(materialsBlock, otherMaterial):
	if otherMaterial:
		otherMatTypeList=re.findall(r'Type\s*of\s*other\s*material\s*(?:\:\s*<>)?([^>]*?)\s*<>',materialsBlock,re.IGNORECASE)
	if not otherMatTypeList:
		otherMatTypeList=re.findall(r'Other\s*type\s*of\s*material\s*(?:\:\s*<>)?([^>]*?)\s*<>',materialsBlock,re.IGNORECASE)
	if otherMatTypeList:
		otherMatType = commonClean(otherMatTypeList[0])
		otherMatType = re.sub(r'^\s*Materials?\s*$',r'',otherMatType,re.IGNORECASE)
		otherMaterial = otherMatType+' - '+otherMaterial
		otherMaterial = re.sub(r'\s*\(e\.g\.\s*[a-z]+\)\s*',r'', otherMaterial, re.IGNORECASE)
	return(otherMaterial)

def otherMaterialKey(materialsBlock):
	otherMatType=""
	otherMatTypeList=re.findall(r'Type\s*of\s*other\s*material\s*(?:\:\s*<>)?([^>]*?)\s*<>',materialsBlock,re.IGNORECASE)
	if not otherMatTypeList:
		otherMatTypeList=re.findall(r'Other\s*type\s*of\s*material\s*(?:\:\s*<>)?([^>]*?)\s*<>',materialsBlock,re.IGNORECASE)
	if otherMatTypeList:
		otherMatType = commonClean(otherMatTypeList[0])
		otherMatType = re.sub(r'^\s*Materials?\s*$',r'',otherMatType,re.IGNORECASE)
		# otherMaterial = otherMatType+' - '+otherMaterial
		# otherMaterial = re.sub(r'\s*\(e\.g\.\s*[a-z]+\)\s*',r'', otherMaterial, re.IGNORECASE)
	return(otherMatType)	

	