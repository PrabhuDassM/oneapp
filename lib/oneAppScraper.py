import re

def scrapePDF(pdfName,textFileName,Format,pdfFilecontent,regexFile,oneAppUtility):
	with open("pdfFilecontent.txt","w") as f:
		f.write(str(pdfFilecontent))
	(siteHouse,siteSuffix,siteHouseName,siteStreet,siteCity,siteCounty,siteCountry,sitePostcode,easting,northing,applicantTitle,applicantFirstname,applicantSurname,applicantCompanyname,applicantStreetAddress,applicantTown,applicantCounty,applicantCountry,applicantPostcode,applicantTelephone,applicantMobile,applicantFax,applicantEmail,agentTitle,agentFirstname,agentSurname,agentCompanyname,agentStreetAddress,agentTown,agentCounty,agentCountry,agentPostcode,agentTelephone,agentMobile,agentFax,agentEmail,proposal,existingWallMaterial,proposedWallMaterial,existingRoofMaterial,proposedRoofMaterial,existingwindowMaterial,proposedwindowMaterial,existingDoorMaterial,proposedDoorMaterial,existingBoundaryTreatmentMaterial,proposedBoundaryTreatmentMaterial,existingVehicleaccessMaterial,proposedVehicleaccessMaterial,existingLightingMaterial,proposedLightingMaterial,existingChimneyMaterial,proposedChimneyMaterial,existingRainwaterGoodsMaterial,proposedRainwaterGoodsMaterial,existingCeilingMaterial,proposedCeilingMaterial,existingFloorMaterial,proposedFloorMaterial,existingotherMaterial,proposedotherMaterial,vehicleCar,vehicleLightGood,vehicleMotorcycle,vehicleDisability,vehicleCycle,vehicleOther,vehicleOtherDesc,existingUse,proposedHouses,proposedFlatsMaisonettes,proposedLiveWorkUnits,proposedClusterFlats,proposedShelteredHousing,proposedBedsitStudios,proposedUnknown,proposedMarketHousingTotal,existingHouses,existingFlatsMaisonettes,existingLiveWorkUnits,existingClusterFlats,existingShelteredHousing,existingBedsitStudios,existingUnknown,existingMarketHousingTotal,totalProposedResidentialUnits,totalExistingResidentialUnits,nonResidentialShops,nonResidentialFinancial,nonResidentialRestaurants,nonResidentialDrinking,nonResidentialFood,nonResidentialOffice,nonResidentialResearch,nonResidentialLight,nonResidentialGeneral,nonResidentialStorage,nonResidentialHotels,nonResidentialInstitutions,nonResidentialResidential,nonResidentialAssembly,nonResidentialOther,nonResidentialTotal,existingEmployee,proposedEmployee,siteArea,Material_Additional_Information,Material_Additional_Info_Details,Existing_Residential_House) = ('','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','')
	if Format == "Format1":
		# Get site address details
		siteAddressBlockRegex=regexFile.get(Format, 'siteAddressBlock')
		siteAddressBlock=re.search(siteAddressBlockRegex, pdfFilecontent, re.I)
		
		if siteAddressBlock:
			siteAddressBlock = siteAddressBlock.group(1)
			with open("siteAddressBlock.txt","w") as f:
				f.write(str(siteAddressBlock))			
			# need to change variable to list
			siteHouse = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'siteHouse1', '', '', '', '')
			siteSuffix = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'siteSuffix1', '', '', '', '')
			siteHouseName = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'siteHouseName1', 'siteHouseName2', '', '', '')
			siteStreet = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'siteStreet1', '', '', '', '')
			siteCity = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'siteCity1', 'siteCity2', '', '', '')
			siteCounty = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'siteCounty1', '', '', '', '')
			siteCountry = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'siteCountry1', '', '', '', '')
			sitePostcode = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'sitePostcode1', '', '', '', '')
			easting = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'easting1', '', '', '', '')
			northing = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'northing1', '', '', '', '')

		# Get applicant contact details
		applicantBlockRegex=regexFile.get(Format, 'applicantBlock')
		applicantBlock=re.search(applicantBlockRegex, pdfFilecontent, re.I)
		if applicantBlock:
			applicantBlock = applicantBlock.group(1)
			with open("applicantBlock.txt","w") as f:
				f.write(str(applicantBlock))
			applicantTitle = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantTitle1', 'applicantTitle2', '', '', '')
			applicantFirstname = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantFirstname1', 'applicantFirstname2', '', '', '')
			applicantSurname = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantSurname1', 'applicantSurname2', '', '', '')
			applicantCompanyname = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantCompanyname1', 'applicantCompanyname2', '', '', '')
			applicantStreetAddress = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantStreetAddress1', '', '', '', '')
			applicantTown = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantTown1', '', '', '', '')
			applicantCounty = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantCounty1', '', '', '', '')
			applicantCountry = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantCountry1', '', '', '', '')
			applicantPostcode = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantPostcode1', 'applicantPostcode2', '', '', '')
			applicantTelephone = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantTelephone1', '', '', '', '')
			applicantMobile = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantMobile1', '', '', '', '')
			applicantFax = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantFax1', '', '', '', '')
			applicantEmail = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantEmail1', 'applicantEmail2', '', '', '')

		# Get agent contact details
		agentBlockRegex=regexFile.get(Format, 'agentBlock')
		agentBlock=re.search(agentBlockRegex, pdfFilecontent, re.I)
		if agentBlock:
			agentBlock = agentBlock.group(1)
			with open("agentBlock.txt","w") as f:
				f.write(str(agentBlock))
			agentTitle = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentTitle1', 'agentTitle2', '', '', '')
			agentFirstname = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentFirstname1', 'agentFirstname2', '', '', '')
			agentSurname = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentSurname1', 'agentSurname2', '', '', '')
			agentCompanyname = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentCompanyname1', 'agentCompanyname2', '', '', '')
			agentStreetAddress = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentStreetAddress1', '', '', '', '')
			agentTown = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentTown1', '', '', '', '')
			agentCounty = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentCounty1', '', '', '', '')
			agentCountry = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentCountry1', '', '', '', '')
			agentPostcode = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentPostcode1', 'agentPostcode2', 'agentPostcode3', 'agentPostcode4', '')
			agentTelephone = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentTelephone1', 'agentTelephone2', 'agentTelephone3', '', '')
			agentMobile = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentMobile1', 'agentMobile2', '', '', '')
			agentFax = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentFax1', 'agentFax2', '', '', '')
			agentEmail = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentEmail1', 'agentEmail2', '', '', '')

		# Get proposal details
		proposalBlockRegex=regexFile.get(Format, 'proposalBlock')
		proposalBlock=re.search(proposalBlockRegex, pdfFilecontent, re.I)
		if proposalBlock:
			proposalBlock = proposalBlock.group(1)
			proposalBlock = re.sub(r'^([\w\W]*?on\s*the\s*decision\s*letter\s*\:(?:<>)?\s*[\w\W]*?)Application\s*reference\s*number[\w\W]*?\s*Condition\s*number\(s\)\:\s*[\d]?\s*[a-z]?\s*\)?\s*([\w\W]*?\s*Has\s*the\s*development\s*already\s*started)', r'\1<>\2',proposalBlock,re.IGNORECASE)
			with open("proposalBlock.txt","w") as f:
				f.write(str(proposalBlock))
			
			proposal = oneAppUtility.scrapeValue(proposalBlock, regexFile, Format, 'proposal1', 'proposal2', 'proposal3', 'proposal4', '')
			if proposal == '':
				proposal = oneAppUtility.scrapeValue(proposalBlock, regexFile, Format, 'proposal5', '', '', '', '')
				print ("proposal5:",proposal)
				if proposal != '':
					
					proposal = oneAppUtility.scrapeValue(proposal, regexFile, Format, 'proposal6', 'proposal7', 'proposal8', 'proposal9', '')
					
					proposal = oneAppUtility.cleanScrapedValue(proposal,regexFile, Format, 'cleanProposal1')
					proposal = oneAppUtility.cleanScrapedValue(proposal,regexFile, Format, 'cleanProposal2')
					proposal = oneAppUtility.cleanScrapedValue(proposal,regexFile, Format, 'cleanProposal3')
					# print ("proposal5:",proposal)
					# raw_input("OneAPP_Proposal")
			
			# print ("proposal:",proposal)
			# raw_input("OneAPP_Proposal")
			

		# Get Vehicle Parking details
		vehicleParkingBlockRegex=regexFile.get(Format, 'vehicleParkingBlock')
		vehicleParkingBlock=re.search(vehicleParkingBlockRegex, pdfFilecontent, re.I)
		if vehicleParkingBlock:
			vehicleParkingBlock = vehicleParkingBlock.group(1)
			# with open("vehicleParkingBlock.txt","w") as f:
			# 	f.write(str(vehicleParkingBlock))
			vehicleCar = oneAppUtility.scrapeValue(vehicleParkingBlock, regexFile, Format, 'vehicleCar', '', '', '', '')
			vehicleCar = re.sub('\s+', '|',vehicleCar)
			vehicleLightGood = oneAppUtility.scrapeValue(vehicleParkingBlock, regexFile, Format, 'vehicleLightGood1', 'vehicleLightGood2', '', '', '')
			vehicleLightGood = re.sub('\s+', '|',vehicleLightGood)
			vehicleMotorcycle = oneAppUtility.scrapeValue(vehicleParkingBlock, regexFile, Format, 'vehicleMotorcycle', '', '', '', '')
			vehicleMotorcycle = re.sub('\s+', '|',vehicleMotorcycle)
			vehicleDisability = oneAppUtility.scrapeValue(vehicleParkingBlock, regexFile, Format, 'vehicleDisability', '', '', '', '')
			vehicleDisability = re.sub('\s+', '|',vehicleDisability)
			vehicleCycle = oneAppUtility.scrapeValue(vehicleParkingBlock, regexFile, Format, 'vehicleCycle', '', '', '', '')
			vehicleCycle = re.sub('\s+', '|',vehicleCycle)
			vehicleOther = oneAppUtility.scrapeValue(vehicleParkingBlock, regexFile, Format, 'vehicleOther', '', '', '', '')
			vehicleOther = re.sub('\s+', '|',vehicleOther)
			vehicleOtherDesc = oneAppUtility.scrapeValue(vehicleParkingBlock, regexFile, Format, 'vehicleOtherDesc', '', '', '', '')
			vehicleOtherDesc = re.sub('\s+', '|',vehicleOtherDesc)

		# Get employment details
		employmentDetailsBlockRegex=regexFile.get(Format, 'employmentDetailsBlock')
		employmentDetailsBlock=re.search(employmentDetailsBlockRegex, pdfFilecontent, re.I)
		if employmentDetailsBlock:
			employmentDetailsBlock = employmentDetailsBlock.group(1)
			with open("employmentDetailsBlock.txt","w") as f:
				f.write(str(employmentDetailsBlock))
			existingEmployee = oneAppUtility.scrapeValue(employmentDetailsBlock, regexFile, Format, 'existingEmployee', '', '', '', '')
			proposedEmployee = oneAppUtility.scrapeValue(employmentDetailsBlock, regexFile, Format, 'proposedEmployee', '', '', '', '')
			existingEmployee = re.sub('\s+', '|',existingEmployee)
			proposedEmployee = re.sub('\s+', '|',proposedEmployee)
			# print ("existingEmployee:",existingEmployee)
			# print ("proposedEmployee:",proposedEmployee)
			
		# Get Site Area details
		siteAreaBlockRegex=regexFile.get(Format, 'siteAreaBlock')
		siteAreaBlock=re.search(siteAreaBlockRegex, pdfFilecontent, re.I)
		if siteAreaBlock:
			siteAreaBlock = siteAreaBlock.group(1)
			with open("siteAreaBlock.txt","w") as f:
				f.write(str(siteAreaBlock))
			siteAreaRegexList=('siteArea1', 'siteArea2', 'siteArea3', 'siteArea4')
			siteAreaList = oneAppUtility.scrapeMultiValue(siteAreaBlock, regexFile, Format, siteAreaRegexList)
			siteArea = " ".join(siteAreaList)
			# print ("siteArea:",siteArea)
			
		# Get existing Use details
		existingUseBlockRegex=regexFile.get(Format, 'existingUseBlock')
		existingUseBlock=re.search(existingUseBlockRegex, pdfFilecontent, re.I)
		if existingUseBlock:
			existingUseBlock = existingUseBlock.group(1)
			with open("existingUseBlock.txt","w") as f:
				f.write(str(existingUseBlock))
			existingUseRegexList=[]
			existingUseRegexList.append('existingUse')
			existingUseList=[]
			existingUseList.append(oneAppUtility.scrapeMultiValue(existingUseBlock, regexFile, Format, existingUseRegexList))
			existingUse = " ".join(existingUseList)
			# print ("existingUse:",existingUse)

		# Get Proposed Residential unit details
		# (proposedHouses, proposedFlatsMaisonettes, proposedLiveWorkUnits, proposedClusterFlats, proposedShelteredHousing, proposedBedsitStudios, proposedUnknown, proposedMarketHousingTotal)=('','','','','','','','')
		residentialUnitsBlockRegex=regexFile.get(Format, 'residentialUnitsBlock')
		residentialUnitsBlock=re.search(residentialUnitsBlockRegex, pdfFilecontent, re.I)
		if residentialUnitsBlock:
			residentialUnitsBlock = residentialUnitsBlock.group(1)
			proposedResidentialUnitsBlockRegex=regexFile.get(Format, 'proposedResidentialUnitsBlock')
			proposedResidentialUnitsBlockList=re.findall(proposedResidentialUnitsBlockRegex, residentialUnitsBlock, re.I)
			if proposedResidentialUnitsBlockList:
				for proposedResidentialUnitsBlock in proposedResidentialUnitsBlockList:
					proposedHouses = proposedHouses+'|'+oneAppUtility.scrapeValue(proposedResidentialUnitsBlock, regexFile, Format, 'Houses', '', '', '', '')
					proposedFlatsMaisonettes = proposedFlatsMaisonettes+'|'+oneAppUtility.scrapeValue(proposedResidentialUnitsBlock, regexFile, Format, 'FlatsMaisonettes', '', '', '', '')
					proposedLiveWorkUnits = proposedLiveWorkUnits+'|'+oneAppUtility.scrapeValue(proposedResidentialUnitsBlock, regexFile, Format, 'LiveWorkUnits', '', '', '', '')
					proposedClusterFlats = proposedClusterFlats+'|'+oneAppUtility.scrapeValue(proposedResidentialUnitsBlock, regexFile, Format, 'ClusterFlats', '', '', '', '')
					proposedShelteredHousing = proposedShelteredHousing+'|'+oneAppUtility.scrapeValue(proposedResidentialUnitsBlock, regexFile, Format, 'ShelteredHousing', '', '', '', '')
					proposedBedsitStudios = proposedBedsitStudios+'|'+oneAppUtility.scrapeValue(proposedResidentialUnitsBlock, regexFile, Format, 'BedsitStudios', '', '', '', '')
					proposedUnknown = proposedUnknown+'|'+oneAppUtility.scrapeValue(proposedResidentialUnitsBlock, regexFile, Format, 'Unknown', '', '', '', '')
					proposedMarketHousingTotal = proposedMarketHousingTotal+'|'+oneAppUtility.scrapeValue(proposedResidentialUnitsBlock, regexFile, Format, 'proposedMarketHousingTotal', '', '', '', '')
				proposedHouses = oneAppUtility.commonClean(re.sub('\s+', '|',proposedHouses))
				proposedFlatsMaisonettes = oneAppUtility.commonClean(re.sub('\s+', '|',proposedFlatsMaisonettes))
				proposedLiveWorkUnits = oneAppUtility.commonClean(re.sub('\s+', '|',proposedLiveWorkUnits))
				proposedClusterFlats = oneAppUtility.commonClean(re.sub('\s+', '|',proposedClusterFlats))
				proposedShelteredHousing = oneAppUtility.commonClean(re.sub('\s+', '|',proposedShelteredHousing))
				proposedBedsitStudios = oneAppUtility.commonClean(re.sub('\s+', '|',proposedBedsitStudios))
				proposedUnknown = oneAppUtility.commonClean(re.sub('\s+', '|',proposedUnknown))
				proposedMarketHousingTotal = oneAppUtility.commonClean(re.sub('\s+', '|',proposedMarketHousingTotal))

			existingResidentialUnitsBlockRegex=regexFile.get(Format, 'existingResidentialUnitsBlock')
			existingResidentialUnitsBlockList=re.findall(existingResidentialUnitsBlockRegex, residentialUnitsBlock, re.I)

			# (existingHouses, existingFlatsMaisonettes, existingLiveWorkUnits, existingClusterFlats, existingShelteredHousing, existingBedsitStudios, existingUnknown, existingMarketHousingTotal)=('','','','','','','','')
			if existingResidentialUnitsBlockList:
				for existingResidentialUnitsBlock in existingResidentialUnitsBlockList:
					existingHouses = existingHouses+'|'+oneAppUtility.scrapeValue(existingResidentialUnitsBlock, regexFile, Format, 'Houses', '', '', '', '')
					existingFlatsMaisonettes = existingFlatsMaisonettes+'|'+oneAppUtility.scrapeValue(existingResidentialUnitsBlock, regexFile, Format, 'FlatsMaisonettes', '', '', '', '')
					existingLiveWorkUnits = existingLiveWorkUnits+'|'+oneAppUtility.scrapeValue(existingResidentialUnitsBlock, regexFile, Format, 'LiveWorkUnits', '', '', '', '')
					existingClusterFlats = existingClusterFlats+'|'+oneAppUtility.scrapeValue(existingResidentialUnitsBlock, regexFile, Format, 'ClusterFlats', '', '', '', '')
					existingShelteredHousing = existingShelteredHousing+'|'+oneAppUtility.scrapeValue(existingResidentialUnitsBlock, regexFile, Format, 'ShelteredHousing', '', '', '', '')
					existingBedsitStudios = existingBedsitStudios+'|'+oneAppUtility.scrapeValue(existingResidentialUnitsBlock, regexFile, Format, 'BedsitStudios', '', '', '', '')
					existingUnknown = existingUnknown+'|'+oneAppUtility.scrapeValue(existingResidentialUnitsBlock, regexFile, Format, 'Unknown', '', '', '', '')
					existingMarketHousingTotal = existingMarketHousingTotal+'|'+oneAppUtility.scrapeValue(existingResidentialUnitsBlock, regexFile, Format, 'existingMarketHousingTotal', '', '', '', '')
				existingHouses = oneAppUtility.commonClean(re.sub('\s+', '|',existingHouses))
				existingFlatsMaisonettes = oneAppUtility.commonClean(re.sub('\s+', '|',existingFlatsMaisonettes))	
				existingLiveWorkUnits = oneAppUtility.commonClean(re.sub('\s+', '|',existingLiveWorkUnits))	
				existingClusterFlats = oneAppUtility.commonClean(re.sub('\s+', '|',existingClusterFlats))	
				existingShelteredHousing = oneAppUtility.commonClean(re.sub('\s+', '|',existingShelteredHousing))	
				existingBedsitStudios = oneAppUtility.commonClean(re.sub('\s+', '|',existingBedsitStudios))	
				existingUnknown = oneAppUtility.commonClean(re.sub('\s+', '|',existingUnknown))	
				existingMarketHousingTotal = oneAppUtility.commonClean(re.sub('\s+', '|',existingMarketHousingTotal))	

			totalResidentialBlockRegex=regexFile.get(Format, 'totalResidentialBlock')
			totalResidentialBlock=re.search(totalResidentialBlockRegex, residentialUnitsBlock, re.I)
			if totalResidentialBlock:
				totalResidentialBlock = totalResidentialBlock.group(1)
				totalProposedResidentialUnits = oneAppUtility.scrapeValue(totalResidentialBlock, regexFile, Format, 'totalProposedResidentialUnits', '', '', '', '')
				totalProposedResidentialUnits = re.sub('\s+', '|',totalProposedResidentialUnits)
				totalExistingResidentialUnits = oneAppUtility.scrapeValue(totalResidentialBlock, regexFile, Format, 'totalExistingResidentialUnits', '', '', '', '')
				totalExistingResidentialUnits = re.sub('\s+', '|',totalExistingResidentialUnits)
				# print ("totalProposedResidentialUnits:",totalProposedResidentialUnits)
				# print ("totalExistingResidentialUnits:",totalExistingResidentialUnits)
		# Get Non Residential unit details
		nonResidentialUnitsBlockRegex=regexFile.get(Format, 'nonResidentialUnitsBlock')
		nonResidentialUnitsBlock=re.search(nonResidentialUnitsBlockRegex, pdfFilecontent, re.I)
		if nonResidentialUnitsBlock:
			nonResidentialUnitsBlock = nonResidentialUnitsBlock.group(1)
			with open("nonResidentialUnitsBlock.txt","w") as f:
				f.write(str(nonResidentialUnitsBlock))
			nonResidentialShops = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialShops', '', '', '', ''))
			nonResidentialFinancial = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialFinancial', '', '', '', ''))
			nonResidentialRestaurants = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialRestaurants', '', '', '', ''))
			nonResidentialDrinking = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialDrinking', '', '', '', ''))
			nonResidentialFood = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialFood', '', '', '', ''))
			nonResidentialOffice = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialOffice1', 'nonResidentialOffice2', '', '', ''))
			nonResidentialResearch = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialResearch', '', '', '', ''))
			nonResidentialLight = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialLight', '', '', '', ''))
			nonResidentialGeneral = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialGeneral', '', '', '', ''))
			nonResidentialStorage = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialStorage', '', '', '', ''))
			nonResidentialHotels = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialHotels', '', '', '', ''))
			nonResidentialInstitutions = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialInstitutions', '', '', '', ''))
			nonResidentialResidential = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialResidential', '', '', '', ''))
			# print "nonResidentialInstitutions::",nonResidentialInstitutions
			# print "nonResidentialResidential::",nonResidentialResidential
			# raw_input("******")
			nonResidentialAssembly = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialAssembly', '', '', '', ''))
			nonResidentialOther = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialOther1', 'nonResidentialOther2', 'nonResidentialOther3', '', ''))
			nonResidentialTotal = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialTotal1', 'nonResidentialTotal2', '', '', ''))


		# Get Non Residential unit details
		materialsBlockRegex=regexFile.get(Format, 'materialsBlock')
		materialsBlock=re.search(materialsBlockRegex, pdfFilecontent, re.I)
		
		materialKeywordsList=('Walls','External Walls','Internal Walls','roof','Roof covering','window','doors','External Doors','boundary treatment','vehicle access','lightning','Lighting','others','Chimney','Ceilings','Floors','Rainwater goods','Vehicle access and hard standing')
		
		otherMatType = ''
		if materialsBlock:
			materialsBlock = materialsBlock.group(1)
			with open("materialsBlock_before.txt","w") as f:
				f.write(str(materialsBlock))
			
			materialsBlockfind = re.findall(r'(Walls|External\s*Walls|Internal\s*Walls|roof|Roof\s*covering|window|doors|External\s*Doors|boundary\s*treatment|vehicle\s*access|lightning|Lighting|others?|Chimney|Ceilings|Floors|Rainwater\s*goods|Vehicle\s*access\s*and\s*hard\s*standing)', materialsBlock, re.I) 
			
			# print "materialsBlockfind:",materialsBlockfind
			
			for foundWord in materialsBlockfind:
				materialsBlock = re.sub(r'(>)\s*('+str(foundWord)+'[^>]*?description\s*\:?)',r'\1\n<M>\2', materialsBlock, re.IGNORECASE)
			
			for eachKeyword in materialKeywordsList:
				materialsBlock = re.sub(r'(?:(<M>'+str(eachKeyword)+'<>[\w\W]*?)<M>'+str(eachKeyword)+'<>)',r'\1', materialsBlock, re.IGNORECASE)
				
			materialsBlock = re.sub(r'<M>\s*<M>',r'<M>', materialsBlock, re.IGNORECASE)
			
			# print "materialsBlock:",materialsBlock
			
			with open("materialsBlock.txt","w") as f:
				f.write(str(materialsBlock))
				
			materialTypeList=('(?:\s*Internal|External)?\s*Walls\s*','\s*Roof(?:\s*covering)?\s*','\s*window(?:s)?\s*','(?:\s*Internal|External)?\s*Door(?:s)?\s*','\s*Vehicle\s*access\s*[^>]*?','\s*(?:Lighting|lightning)\s*','\s*Boundary\s*treatment[^>]*?\s*','\s*Chimney[^>]*?\s*','\s*Rainwater\s*good(?:s)?[^>]*?\s*','\s*Ceiling(?:s)?[^>]*?\s*','\s*Floor(?:s)?[^>]*?\s*','\s*other(?:s)??[^>]*?\s*')	
			
			
			# (existingWallMaterial, proposedWallMaterial, existingRoofMaterial, proposedRoofMaterial, existingwindowMaterial, proposedwindowMaterial, existingDoorMaterial, proposedDoorMaterial, existingVehicleaccessMaterial, proposedVehicleaccessMaterial, existingLightingMaterial, proposedLightingMaterial, existingBoundaryTreatmentMaterial, proposedBoundaryTreatmentMaterial, existingChimneyMaterial, proposedChimneyMaterial, existingRainwaterGoodsMaterial, proposedRainwaterGoodsMaterial, existingCeilingMaterial, proposedCeilingMaterial, existingFloorMaterial, proposedFloorMaterial, existingotherMaterial, proposedotherMaterial) = ('','','','','','','','','','','','','','','','','','','','','','','','')
			
			
			for materialType in materialTypeList:
			
				# print "materialType:",materialType
				# print "materialsBlock:",materialsBlock
				
				if 'Wall' in materialType:
					(existingWallMaterial, proposedWallMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Roof' in materialType:
					(existingRoofMaterial, proposedRoofMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'window' in materialType:
					(existingwindowMaterial, proposedwindowMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Door' in materialType:
					(existingDoorMaterial, proposedDoorMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Vehicle' in materialType:
					(existingVehicleaccessMaterial, proposedVehicleaccessMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Lighting|lightning' in materialType:
					(existingLightingMaterial, proposedLightingMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Boundary' in materialType:
					(existingBoundaryTreatmentMaterial, proposedBoundaryTreatmentMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Chimney' in materialType:
					(existingChimneyMaterial, proposedChimneyMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Rainwater' in materialType:
					(existingRainwaterGoodsMaterial, proposedRainwaterGoodsMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Ceiling' in materialType:
					(existingCeilingMaterial, proposedCeilingMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Floor' in materialType:
					(existingFloorMaterial, proposedFloorMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'other' in materialType:
					(existingotherMaterial, proposedotherMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
					
					otherMatType = oneAppUtility.otherMaterialKey(materialsBlock)
					
					if otherMatType:
						if (otherMatType.lower().find('wall') != -1):
							(existingWallMaterial,proposedWallMaterial,existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingWallMaterial,proposedWallMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('roof') != -1):
								(existingRoofMaterial, proposedRoofMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingRoofMaterial, proposedRoofMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('window') != -1):
								(existingwindowMaterial, proposedwindowMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingwindowMaterial, proposedwindowMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('door') != -1):
								(existingDoorMaterial, proposedDoorMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingDoorMaterial, proposedDoorMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('vehicle') != -1):
								(existingVehicleaccessMaterial, proposedVehicleaccessMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingVehicleaccessMaterial, proposedVehicleaccessMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('lightning') != -1):
								(existingLightingMaterial, proposedLightingMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingLightingMaterial, proposedLightingMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('boundary') != -1):
								(existingBoundaryTreatmentMaterial, proposedBoundaryTreatmentMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingBoundaryTreatmentMaterial, proposedBoundaryTreatmentMaterial,existingotherMaterial, proposedotherMaterial)
						if (otherMatType.lower().find('rainwater') != -1): 
								
								(existingRainwaterGoodsMaterial, proposedRainwaterGoodsMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingRainwaterGoodsMaterial, proposedRainwaterGoodsMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('ceiling') != -1):
								(existingCeilingMaterial, proposedCeilingMaterial,existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingCeilingMaterial, proposedCeilingMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('floor') != -1):
								(existingFloorMaterial, proposedFloorMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingFloorMaterial, proposedFloorMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('chimney') != -1):
								(existingChimneyMaterial, proposedChimneyMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingChimneyMaterial, proposedChimneyMaterial,existingotherMaterial, proposedotherMaterial)
			if existingotherMaterial:
					existingotherMaterial = oneAppUtility.addOtherMaterialType(materialsBlock, existingotherMaterial)
			if proposedotherMaterial:
					proposedotherMaterial = oneAppUtility.addOtherMaterialType(materialsBlock, proposedotherMaterial)

	elif Format == "Format2": # Format 2 application scraping started
		siteAddressBlockRegex=regexFile.get(Format, 'siteAddressBlock')
		siteAddressBlock=re.search(siteAddressBlockRegex, pdfFilecontent, re.I)
		if siteAddressBlock:
			siteAddressBlock = siteAddressBlock.group(1)
			with open("siteAddressBlock.txt","w") as f:
				f.write(str(siteAddressBlock))
			# need to change variable to list
			siteStreetList = []
			siteHouse = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'siteHouse1', 'siteHouse2', '', '', '')
			siteSuffix = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'siteSuffix1', '', '', '', '')
			siteHouseName = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'siteHouseName1', 'siteHouseName2', '', '', '')
			siteStreet = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'siteStreet1', 'siteStreet2', '', '', '')
			if siteStreet:
				siteStreetList.append(siteStreet)
			siteStreet = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'siteStreet3', 'siteStreet4', '', '', '')
			if siteStreet:
				siteStreetList.append(siteStreet)
			siteStreet = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'siteStreet5', 'siteStreet6', '', '', '')
			if siteStreet:
				siteStreetList.append(siteStreet)
			siteStreet = ", ".join(siteStreetList)
			siteCity = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'siteCity1', 'siteCity1', 'siteCity3', '', '')
			siteCounty = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'siteCounty1', '', '', '', '')
			siteCountry = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'siteCountry1', '', '', '', '')
			sitePostcode = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'sitePostcode1', 'sitePostcode2', '', '', '')
			easting = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'easting1', 'easting2', 'easting3', '', '')
			northing = oneAppUtility.scrapeValue(siteAddressBlock, regexFile, Format, 'northing1', 'northing2', 'northing3', '', '')

		# Get applicant contact details - Format 2
		applicantBlockRegex=regexFile.get(Format, 'applicantBlock')
		applicantBlock=re.search(applicantBlockRegex, pdfFilecontent, re.I)
		# print ("applicantBlock:",applicantBlock)
		applicantStreetAddressList=[]
		if applicantBlock:
			applicantBlock = applicantBlock.group(1)
			applicantBlock=re.sub(r'[\d]+\.\s+Applicant\s*Details\s*',r'', applicantBlock, re.IGNORECASE)
			with open("applicantBlock.txt","w") as f:
				f.write(str(applicantBlock))
			applicantTitle = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantTitle1', 'applicantTitle2', '', '', '')
			# print ("applicantTitle",applicantTitle)
			applicantFirstname = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantFirstname1', 'applicantFirstname2', '', '', '')
			applicantSurname = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantSurname1', 'applicantSurname2', '', '', '')
			applicantCompanyname = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantCompanyname1', 'applicantCompanyname2', 'applicantCompanyname3', '', '')
			applicantStreetAddress = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantStreetAddress1', 'applicantStreetAddress2', '', '', '')
			if applicantStreetAddress:
				applicantStreetAddressList.append(applicantStreetAddress)
			applicantStreetAddress = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantStreetAddress3', 'applicantStreetAddress4', '', '', '')
			if applicantStreetAddress:
				applicantStreetAddressList.append(applicantStreetAddress)
			applicantStreetAddress = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantStreetAddress5', 'applicantStreetAddress6', '', '', '')
			if applicantStreetAddress:
				applicantStreetAddressList.append(applicantStreetAddress)
			applicantStreetAddress = ", ".join(applicantStreetAddressList)
			applicantTown = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantTown1', 'applicantTown2', 'applicantTown3', '', '')
			applicantCounty = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantCounty1', '', '', '', '')
			# print ("Applicant_County",applicantCounty)
			# raw_input("applicantCountyStop")
			applicantCountry = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantCountry1', 'applicantCountry2', 'applicantCountry3', '', '')
			applicantPostcode = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantPostcode1', 'applicantPostcode2', '', '', '')
			applicantTelephone = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantTelephone1', 'applicantTelephone2', 'applicantTelephone3', '', '')
			applicantMobile = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantMobile1', '', '', '', '')
			applicantFax = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantFax1', 'applicantFax2', '', '', '')
			applicantEmail = oneAppUtility.scrapeValue(applicantBlock, regexFile, Format, 'applicantEmail1', '', '', '', '')

		# Get agent contact details - Format 2
		agentBlockRegex=regexFile.get(Format, 'agentBlock')
		agentBlock=re.search(agentBlockRegex, pdfFilecontent, re.I)
		if agentBlock:
			agentBlock = agentBlock.group(1)
			with open("agentBlock.txt","w") as f:
				f.write(str(agentBlock))
			agentStreetAddressList=[]
			agentTitle = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentTitle1', 'agentTitle2', '', '', '')
			agentFirstname = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentFirstname1', 'agentFirstname2', '', '', '')
			agentSurname = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentSurname1', 'agentSurname2', '', '', '')
			agentCompanyname = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentCompanyname1', 'agentCompanyname2', 'agentCompanyname3', '', '')
			agentStreetAddress = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentStreetAddress1', '', '', '', '')
			agentStreetAddress = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentStreetAddress1', 'agentStreetAddress2', '', '', '')
			if agentStreetAddress:
				agentStreetAddressList.append(agentStreetAddress)
			agentStreetAddress = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentStreetAddress3', 'agentStreetAddress4', '', '', '')
			if agentStreetAddress:
				agentStreetAddressList.append(agentStreetAddress)
			agentStreetAddress = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentStreetAddress5', 'agentStreetAddress6', '', '', '')
			if agentStreetAddress:
				agentStreetAddressList.append(agentStreetAddress)
			agentStreetAddress = ", ".join(agentStreetAddressList)
			agentTown = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentTown1', 'agentTown2', '', '', '')
			agentCounty = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentCounty1', '', '', '', '')
			agentCountry = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentCountry1', 'agentCountry2', '', '', '')
			agentPostcode = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentPostcode1', 'agentPostcode2', 'agentPostcode3', '', '')
			
			agentTelephone = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentTelephone1', 'agentTelephone2', 'agentTelephone3', '', '')
			agentMobile = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentMobile1', '', '', '', '')
			agentFax = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentFax1', 'agentFax2', '', '', '')
			agentEmail = oneAppUtility.scrapeValue(agentBlock, regexFile, Format, 'agentEmail1', '', '', '', '')

		# Get Site Area details - Format 2
		siteAreaBlockRegex=regexFile.get(Format, 'siteAreaBlock')
		siteAreaBlock=re.search(siteAreaBlockRegex, pdfFilecontent, re.I)
		if siteAreaBlock:
			siteAreaBlock = siteAreaBlock.group(1)
			with open("siteAreaBlock.txt","w") as f:
				f.write(str(siteAreaBlock))
			siteAreaRegexList=('siteArea1', 'siteArea2', '', '')
			siteAreaList = oneAppUtility.scrapeMultiValue(siteAreaBlock, regexFile, Format, siteAreaRegexList)
			siteArea = oneAppUtility.commonClean(" ".join(siteAreaList))
			# print ("siteArea:",siteArea)

		# Get proposal details
		proposalBlockRegex=regexFile.get(Format, 'proposalBlock')
		proposalBlock=re.search(proposalBlockRegex, pdfFilecontent, re.I)
		if proposalBlock:
			proposalBlock = proposalBlock.group(1)
			try:
				proposal_head=re.search(r'^\s*[\d]+\.\s*(Description[^>]*?Prop[^>]*?)Please', proposalBlock, re.I)
				proposal_head = proposal_head.group(1)
				proposal_head=re.sub(r'^\s*',r'',proposal_head,re.IGNORECASE)
				proposal_head=re.sub(r'\s*$',r'',proposal_head,re.IGNORECASE)
				proposalBlock=re.sub(r'[\d]+\.\s*'+proposal_head+'\s*',r'',proposalBlock,re.IGNORECASE)
			except:
				proposalBlock=re.sub(r'[\d]+\.\s*Description\s*of\s*the\s*Proposal\s*',r'',proposalBlock,re.IGNORECASE)
				proposalBlock=re.sub(r'[\d]+\.\s*Description\s*of\s*Proposed\s*Works\s*',r'',proposalBlock,re.IGNORECASE)
			
			if proposalBlock != '':
				proposalBlock = oneAppUtility.cleanScrapedValue(proposalBlock,regexFile, Format, 'cleanProposal1')
				proposalBlock = oneAppUtility.cleanScrapedValue(proposalBlock,regexFile, Format, 'cleanProposal2')
				proposalBlock = oneAppUtility.cleanScrapedValue(proposalBlock,regexFile, Format, 'cleanProposal3')
				proposalBlock = oneAppUtility.cleanScrapedValue(proposalBlock,regexFile, Format, 'cleanProposal4')
				proposalBlock = oneAppUtility.cleanScrapedValue(proposalBlock,regexFile, Format, 'cleanProposal5')
			with open("proposalBlock.txt","w") as f:
				f.write(str(proposalBlock))
			proposal = oneAppUtility.scrapeValue(proposalBlock, regexFile, Format, 'proposal2', 'proposal1', '', '', '')
			print ("proposal:",proposal)
			raw_input("SSSSSSSSSSSS")
			
		# Get existing Use details
		existingUseBlockRegex=regexFile.get(Format, 'existingUseBlock')
		existingUseBlock=re.search(existingUseBlockRegex, pdfFilecontent, re.I)
		if existingUseBlock:
			existingUseBlock = existingUseBlock.group(1)
			with open("existingUseBlock.txt","w") as f:
				f.write(str(existingUseBlock))
			existingUseRegexList=[]
			existingUseRegexList.append('existingUse')
			existingUseList=[]
			existingUseList.append(oneAppUtility.scrapeMultiValue(existingUseBlock, regexFile, Format, existingUseRegexList))
			existingUse = " ".join(existingUseList)
			existingUse = oneAppUtility.cleanScrapedValue(existingUse,regexFile, Format, 'cleanexistingUse1')
			existingUse = oneAppUtility.cleanScrapedValue(existingUse,regexFile, Format, 'cleanexistingUse2')
			existingUse = oneAppUtility.commonClean(existingUse)
			# print ("existingUse:",existingUse)

		# Get Vehicle Parking details
		vehicleParkingBlockRegex=regexFile.get(Format, 'vehicleParkingBlock')
		vehicleParkingBlock=re.search(vehicleParkingBlockRegex, pdfFilecontent, re.I)
		if vehicleParkingBlock:
			vehicleParkingBlock = vehicleParkingBlock.group(1)
			# with open("vehicleParkingBlock.txt","w") as f:
			# 	f.write(str(vehicleParkingBlock))
			vehicleCar = oneAppUtility.scrapeValue(vehicleParkingBlock, regexFile, Format, 'vehicleCar', '', '', '', '')
			vehicleCar = re.sub('\s+', '|',vehicleCar)
			vehicleLightGood = oneAppUtility.scrapeValue(vehicleParkingBlock, regexFile, Format, 'vehicleLightGood1', 'vehicleLightGood2', '', '', '')
			vehicleLightGood = re.sub('\s+', '|',vehicleLightGood)
			vehicleMotorcycle = oneAppUtility.scrapeValue(vehicleParkingBlock, regexFile, Format, 'vehicleMotorcycle', '', '', '', '')
			vehicleMotorcycle = re.sub('\s+', '|',vehicleMotorcycle)
			vehicleDisability = oneAppUtility.scrapeValue(vehicleParkingBlock, regexFile, Format, 'vehicleDisability', '', '', '', '')
			vehicleDisability = re.sub('\s+', '|',vehicleDisability)
			vehicleCycle = oneAppUtility.scrapeValue(vehicleParkingBlock, regexFile, Format, 'vehicleCycle', '', '', '', '')
			vehicleCycle = re.sub('\s+', '|',vehicleCycle)
			vehicleOther = oneAppUtility.scrapeValue(vehicleParkingBlock, regexFile, Format, 'vehicleOther', '', '', '', '')
			vehicleOther = re.sub('\s+', '|',vehicleOther)
			vehicleOtherDesc = oneAppUtility.scrapeValue(vehicleParkingBlock, regexFile, Format, 'vehicleOtherDesc', '', '', '', '')
			vehicleOtherDesc = re.sub('\s+', '|',vehicleOtherDesc)

		# Get employment details
		employmentDetailsBlockRegex=regexFile.get(Format, 'employmentDetailsBlock')
		employmentDetailsBlock=re.search(employmentDetailsBlockRegex, pdfFilecontent, re.I)
		if employmentDetailsBlock:
			employmentDetailsBlock = employmentDetailsBlock.group(1)
			with open("employmentDetailsBlock.txt","w") as f:
				f.write(str(employmentDetailsBlock))
			existingEmployee = oneAppUtility.scrapeValue(employmentDetailsBlock, regexFile, Format, 'existingEmployee', '', '', '', '')
			proposedEmployee = oneAppUtility.scrapeValue(employmentDetailsBlock, regexFile, Format, 'proposedEmployee', '', '', '', '')
			existingEmployee = re.sub('\s+', '|',existingEmployee)
			proposedEmployee = re.sub('\s+', '|',proposedEmployee)

		# Get Proposed Residential unit details
		residentialUnitsBlockRegex=regexFile.get(Format, 'residentialUnitsBlock')
		residentialUnitsBlock=re.search(residentialUnitsBlockRegex, pdfFilecontent, re.I)
		if residentialUnitsBlock:
			residentialUnitsBlock = residentialUnitsBlock.group(1)
			with open("residentialUnitsBlock.txt","w") as f:
				f.write(str(residentialUnitsBlock))
			proposedResidentialUnitsBlockRegex=regexFile.get(Format, 'proposedResidentialUnitsBlock')
			proposedResidentialUnitsBlockList=re.findall(proposedResidentialUnitsBlockRegex, residentialUnitsBlock, re.I)
			if proposedResidentialUnitsBlockList:
				for proposedResidentialUnitsBlock in proposedResidentialUnitsBlockList:
					proposedHouses = proposedHouses+'|'+oneAppUtility.scrapeValue(proposedResidentialUnitsBlock, regexFile, Format, 'proposedHouses', '', '', '', '')
					proposedFlatsMaisonettes = proposedFlatsMaisonettes+'|'+oneAppUtility.scrapeValue(proposedResidentialUnitsBlock, regexFile, Format, 'proposedFlatsMaisonettes', '', '', '', '')
					proposedLiveWorkUnits = proposedLiveWorkUnits+'|'+oneAppUtility.scrapeValue(proposedResidentialUnitsBlock, regexFile, Format, 'proposedLiveWorkUnits', '', '', '', '')
					proposedClusterFlats = proposedClusterFlats+'|'+oneAppUtility.scrapeValue(proposedResidentialUnitsBlock, regexFile, Format, 'proposedClusterFlats', '', '', '', '')
					proposedShelteredHousing = proposedShelteredHousing+'|'+oneAppUtility.scrapeValue(proposedResidentialUnitsBlock, regexFile, Format, 'proposedShelteredHousing', '', '', '', '')
					proposedBedsitStudios = proposedBedsitStudios+'|'+oneAppUtility.scrapeValue(proposedResidentialUnitsBlock, regexFile, Format, 'proposedBedsitStudios', '', '', '', '')
					# print ("proposedBedsitStudios",proposedBedsitStudios)
					# raw_input("proposedBedsitStudiosStop")
					proposedUnknown = proposedUnknown+'|'+oneAppUtility.scrapeValue(proposedResidentialUnitsBlock, regexFile, Format, 'proposedUnknown', '', '', '', '')
					proposedMarketHousingTotal = proposedMarketHousingTotal+'|'+oneAppUtility.scrapeValue(proposedResidentialUnitsBlock, regexFile, Format, 'proposedMarketHousingTotal1', 'proposedMarketHousingTotal2', '', '', '')
				proposedHouses = oneAppUtility.commonClean(re.sub('\s+', '|',proposedHouses))
				proposedFlatsMaisonettes = oneAppUtility.commonClean(re.sub('\s+', '|',proposedFlatsMaisonettes))
				proposedLiveWorkUnits = oneAppUtility.commonClean(re.sub('\s+', '|',proposedLiveWorkUnits))
				proposedClusterFlats = oneAppUtility.commonClean(re.sub('\s+', '|',proposedClusterFlats))
				proposedShelteredHousing = oneAppUtility.commonClean(re.sub('\s+', '|',proposedShelteredHousing))
				proposedBedsitStudios = oneAppUtility.commonClean(re.sub('\s+', '|',proposedBedsitStudios))
				proposedUnknown = oneAppUtility.commonClean(re.sub('\s+', '|',proposedUnknown))
				proposedMarketHousingTotal = oneAppUtility.commonClean(re.sub('\s+', '|',proposedMarketHousingTotal))
				
				

			# Get existing Residential unit details
			existingResidentialUnitsBlockRegex=regexFile.get(Format, 'existingResidentialUnitsBlock')
			existingResidentialUnitsBlockList=re.findall(existingResidentialUnitsBlockRegex, residentialUnitsBlock, re.I)

			# (existingHouses, existingFlatsMaisonettes, existingLiveWorkUnits, existingClusterFlats, existingShelteredHousing, existingBedsitStudios, existingUnknown, existingMarketHousingTotal)=('','','','','','','','')
			if existingResidentialUnitsBlockList:
				for existingResidentialUnitsBlock in existingResidentialUnitsBlockList:
					existingHouses = existingHouses+'|'+oneAppUtility.scrapeValue(existingResidentialUnitsBlock, regexFile, Format, 'ExistingHouses', '', '', '', '')
					existingFlatsMaisonettes = existingFlatsMaisonettes+'|'+oneAppUtility.scrapeValue(existingResidentialUnitsBlock, regexFile, Format, 'ExistingFlatsMaisonettes1', 'ExistingFlatsMaisonettes2', '', '', '')
					existingLiveWorkUnits = existingLiveWorkUnits+'|'+oneAppUtility.scrapeValue(existingResidentialUnitsBlock, regexFile, Format, 'ExistingLiveWorkUnits', '', '', '', '')
					existingClusterFlats = existingClusterFlats+'|'+oneAppUtility.scrapeValue(existingResidentialUnitsBlock, regexFile, Format, 'ExistingClusterFlats', '', '', '', '')
					existingShelteredHousing = existingShelteredHousing+'|'+oneAppUtility.scrapeValue(existingResidentialUnitsBlock, regexFile, Format, 'ExistingShelteredHousing', '', '', '', '')
					existingBedsitStudios = existingBedsitStudios+'|'+oneAppUtility.scrapeValue(existingResidentialUnitsBlock, regexFile, Format, 'ExistingBedsitStudios', '', '', '', '')
					existingUnknown = existingUnknown+'|'+oneAppUtility.scrapeValue(existingResidentialUnitsBlock, regexFile, Format, 'ExistingUnknown', '', '', '', '')
					existingMarketHousingTotal = existingMarketHousingTotal+'|'+oneAppUtility.scrapeValue(existingResidentialUnitsBlock, regexFile, Format, 'ExistingMarketHousingTotal', '', '', '', '')
				existingHouses = oneAppUtility.commonClean(re.sub('\s+', '|',existingHouses))
				existingFlatsMaisonettes = oneAppUtility.commonClean(re.sub('\s+', '|',existingFlatsMaisonettes))	
				existingLiveWorkUnits = oneAppUtility.commonClean(re.sub('\s+', '|',existingLiveWorkUnits))	
				existingClusterFlats = oneAppUtility.commonClean(re.sub('\s+', '|',existingClusterFlats))	
				existingShelteredHousing = oneAppUtility.commonClean(re.sub('\s+', '|',existingShelteredHousing))	
				existingBedsitStudios = oneAppUtility.commonClean(re.sub('\s+', '|',existingBedsitStudios))	
				existingUnknown = oneAppUtility.commonClean(re.sub('\s+', '|',existingUnknown))	
				existingMarketHousingTotal = oneAppUtility.commonClean(re.sub('\s+', '|',existingMarketHousingTotal))	

			if residentialUnitsBlock:
				totalProposedResidentialUnits = oneAppUtility.scrapeValue(residentialUnitsBlock, regexFile, Format, 'totalProposedResidentialUnits', '', '', '', '')
				totalProposedResidentialUnits = re.sub('\s+', '|',totalProposedResidentialUnits)
				totalExistingResidentialUnits = oneAppUtility.scrapeValue(residentialUnitsBlock, regexFile, Format, 'totalExistingResidentialUnits1', 'totalExistingResidentialUnits2', '', '', '')
				totalExistingResidentialUnits = re.sub('\s+', '|',totalExistingResidentialUnits)
			
			# print ("totalProposedResidentialUnits::",totalProposedResidentialUnits)
			# raw_input("totalProposedResidentialUnits")
		# Get Non Residential unit details - Format2
		nonResidentialUnitsBlockRegex=regexFile.get(Format, 'nonResidentialUnitsBlock')
		nonResidentialUnitsBlock=re.search(nonResidentialUnitsBlockRegex, pdfFilecontent, re.I)
		if nonResidentialUnitsBlock:
			nonResidentialUnitsBlock = nonResidentialUnitsBlock.group(1)
			with open("nonResidentialUnitsBlock.txt","w") as f:
				f.write(str(nonResidentialUnitsBlock))
			nonResidentialShops = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialShops', '', '', '', ''))
			nonResidentialFinancial = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialFinancial', '', '', '', ''))
			nonResidentialRestaurants = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialRestaurants', '', '', '', ''))
			nonResidentialDrinking = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialDrinking', '', '', '', ''))
			nonResidentialFood = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialFood', '', '', '', ''))
			nonResidentialOffice = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialOffice1', 'nonResidentialOffice2', '', '', ''))
			nonResidentialResearch = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialResearch', '', '', '', ''))
			nonResidentialLight = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialLight', '', '', '', ''))
			nonResidentialGeneral = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialGeneral', '', '', '', ''))
			nonResidentialStorage = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialStorage', '', '', '', ''))
			nonResidentialHotels = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialHotels', '', '', '', ''))
			nonResidentialInstitutions = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialInstitutions', '', '', '', ''))
			nonResidentialUnitsBlock=re.sub(r'Non\-residential\s*institutions\s*([\-\d\s\.\,<>]+)','',nonResidentialUnitsBlock,re.I)
			nonResidentialResidential = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialResidential', '', '', '', ''))
			nonResidentialAssembly = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialAssembly', '', '', '', ''))
			nonResidentialOther = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialOther1', 'nonResidentialOther2', 'nonResidentialOther3', '', ''))
			nonResidentialTotal = re.sub('\s+', '|', oneAppUtility.scrapeValue(nonResidentialUnitsBlock, regexFile, Format, 'nonResidentialTotal1', 'nonResidentialTotal2', '', '', ''))

		# Get Non Residential unit details
		materialsBlockRegex=regexFile.get(Format, 'materialsBlock')
		materialsBlock=re.search(materialsBlockRegex, pdfFilecontent, re.I)
		materialKeywordsList=('Walls','External Walls','Internal Walls','roof','Roof covering','window','doors','External Doors','boundary treatment','vehicle access','lightning','Lighting','others','Chimney','Ceilings','Floors','Rainwater goods','Vehicle access and hard standing')
		if materialsBlock:
			materialsBlock = materialsBlock.group(1)
			materialsBlock = re.sub(r'[\d]+\.\s+Materials',r'', materialsBlock, re.IGNORECASE)
			materialsBlock = re.sub(r'Planning\s*Portal\s*Reference\s*\:[^>]*?<>',r'', materialsBlock, re.IGNORECASE)
			materialsBlock = re.sub(r'\n','', materialsBlock)
			with open("materialsBlock_before.txt","w") as f:
				f.write(str(materialsBlock))
			materialsBlockfind = re.findall(r'(Walls|External\s*Walls|Internal\s*Walls|roof|Roof\s*covering|window|doors|External\s*Doors|Internal\s*Doors|boundary\s*treatment|vehicle\s*access|lightning|Lighting|others?|Chimney|Ceilings|Floors|Rainwater\s*goods|Vehicle\s*access\s*and\s*hard\s*standing)', materialsBlock, re.I) 
			# print ("materialsBlockfind:",materialsBlockfind)
			# print ("materialsBlock:",materialsBlock)
			for foundWord in materialsBlockfind:
				# print ("foundWord:",foundWord)
				# materialsBlock = re.sub(r'(<>)\s*('+str(foundWord)+'[^>]*?<>[^>]*?(?:D|d)escription[^>]*?<>)',r'\1\n<M>\2', materialsBlock, re.IGNORECASE)
				materialsBlock = re.sub(r'(<>)\s*('+str(foundWord)+'[^>]*?<>[\w\W]*?(?:D|d)escription[^>]*?<>)',r'\1\n<M>\2', materialsBlock, re.IGNORECASE)
				
				
			materialsBlock = re.sub(r'(>)\s*(Other[^>]*?<>[^>]*?(?:D|d)escription[^>]*?<>)',r'\1\n<M>\2', materialsBlock, re.IGNORECASE)
			materialsBlock = re.sub(r'<M>\s*<M>',r'<M>', materialsBlock, re.IGNORECASE)
			
			for eachKeyword in materialKeywordsList:
				materialsBlock = re.sub(r'(?:(<M>'+str(eachKeyword)+'<>[\w\W]*?)<M>'+str(eachKeyword)+'<>)',r'\1', materialsBlock, re.IGNORECASE)
			
			# print "materialsBlock",materialsBlock
			# raw_input("**----------------------------------------*")
			
			materialsBlock = re.sub(r'<M>\s*<M>',r'<M>', materialsBlock, re.IGNORECASE)
			
			
			# print "materialsBlock",materialsBlock
			# raw_input("**-----*")
			with open("materialsBlock.txt","w") as f:
				f.write(str(materialsBlock))
			materialTypeList=('(?:\s*Internal|External)?\s*Walls\s*','\s*Roof(?:\s*covering)?\s*','\s*window(?:s)?\s*','(?:\s*Internal|External)?\s*Door(?:s)?\s*','\s*Vehicle\s*access\s*[^>]*?','\s*(?:Lighting|lightning)\s*','\s*Boundary\s*treatment[^>]*?\s*','\s*Chimney[^>]*?\s*','\s*Rainwater\s*good(?:s)?[^>]*?\s*','\s*Ceiling(?:s)?[^>]*?\s*','\s*Floor(?:s)?[^>]*?\s*','\s*other(?:s)??[^>]*?\s*')			
			
			# (existingWallMaterial, proposedWallMaterial, existingRoofMaterial, proposedRoofMaterial, existingwindowMaterial, proposedwindowMaterial, existingDoorMaterial, proposedDoorMaterial, existingVehicleaccessMaterial, proposedVehicleaccessMaterial, existingLightingMaterial, proposedLightingMaterial, existingBoundaryTreatmentMaterial, proposedBoundaryTreatmentMaterial, existingChimneyMaterial, proposedChimneyMaterial, existingRainwaterGoodsMaterial, proposedRainwaterGoodsMaterial, existingCeilingMaterial, proposedCeilingMaterial, existingFloorMaterial, proposedFloorMaterial, existingotherMaterial, proposedotherMaterial) = ('','','','','','','','','','','','','','','','','','','','','','','','')
			for materialType in materialTypeList:
				if 'Wall' in materialType:
					(existingWallMaterial, proposedWallMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Roof' in materialType:
					(existingRoofMaterial, proposedRoofMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'window' in materialType:
					(existingwindowMaterial, proposedwindowMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Door' in materialType:
					(existingDoorMaterial, proposedDoorMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Vehicle' in materialType:
					(existingVehicleaccessMaterial, proposedVehicleaccessMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Lighting|lightning' in materialType:
					(existingLightingMaterial, proposedLightingMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Boundary' in materialType:
					(existingBoundaryTreatmentMaterial, proposedBoundaryTreatmentMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Chimney' in materialType:
					(existingChimneyMaterial, proposedChimneyMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Rainwater' in materialType:
					(existingRainwaterGoodsMaterial, proposedRainwaterGoodsMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Ceiling' in materialType:
					(existingCeilingMaterial, proposedCeilingMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'Floor' in materialType:
					(existingFloorMaterial, proposedFloorMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
				if 'other' in materialType:
					(existingotherMaterial, proposedotherMaterial, materialDetails) = oneAppUtility.scrapeMaterials(materialsBlock, materialType, regexFile, Format)
					# print ("existingotherMaterial:",existingotherMaterial)
					# print ("materialType:",materialType)
					
					
					otherMatType = oneAppUtility.otherMaterialKey(materialsBlock)
					
					# print ("materialDetails:",materialDetails)
					
					if otherMatType:
						if (otherMatType.lower().find('wall') != -1):
							(existingWallMaterial,proposedWallMaterial,existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingWallMaterial,proposedWallMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('roof') != -1):
								(existingRoofMaterial, proposedRoofMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingRoofMaterial, proposedRoofMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('window') != -1):
								(existingwindowMaterial, proposedwindowMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingwindowMaterial, proposedwindowMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('door') != -1):
								(existingDoorMaterial, proposedDoorMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingDoorMaterial, proposedDoorMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('vehicle') != -1):
								(existingVehicleaccessMaterial, proposedVehicleaccessMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingVehicleaccessMaterial, proposedVehicleaccessMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('lightning') != -1):
								(existingLightingMaterial, proposedLightingMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingLightingMaterial, proposedLightingMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('boundary') != -1):
								(existingBoundaryTreatmentMaterial, proposedBoundaryTreatmentMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingBoundaryTreatmentMaterial, proposedBoundaryTreatmentMaterial,existingotherMaterial, proposedotherMaterial)
						if (otherMatType.lower().find('rainwater') != -1): 
								
								(existingRainwaterGoodsMaterial, proposedRainwaterGoodsMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingRainwaterGoodsMaterial, proposedRainwaterGoodsMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('ceiling') != -1):
								(existingCeilingMaterial, proposedCeilingMaterial,existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingCeilingMaterial, proposedCeilingMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('floor') != -1):
								(existingFloorMaterial, proposedFloorMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingFloorMaterial, proposedFloorMaterial,existingotherMaterial, proposedotherMaterial)
						
						if (otherMatType.lower().find('chimney') != -1):
								(existingChimneyMaterial, proposedChimneyMaterial, existingotherMaterial, proposedotherMaterial) = oneAppUtility.checkMaterialOthers(existingChimneyMaterial, proposedChimneyMaterial,existingotherMaterial, proposedotherMaterial)
			if existingotherMaterial:
					existingotherMaterial = oneAppUtility.addOtherMaterialType(materialsBlock, existingotherMaterial)
			if proposedotherMaterial:
					proposedotherMaterial = oneAppUtility.addOtherMaterialType(materialsBlock, proposedotherMaterial)
			
			# print "existingotherMaterial",existingotherMaterial
			# print "proposedotherMaterial",proposedotherMaterial
			# raw_input("***************")
	buildInsertQuery=str(siteHouse)+"','"+str(siteSuffix)+"','"+str(siteHouseName)+"','"+str(siteStreet)+"','"+str(siteCity)+"','"+str(siteCounty)+"','"+str(siteCountry)+"','"+str(sitePostcode)+"','"+str(easting)+"','"+str(northing)+"','"+str(applicantTitle)+"','"+str(applicantFirstname)+"','"+str(applicantSurname)+"','"+str(applicantCompanyname)+"','"+str(applicantStreetAddress)+"','"+str(applicantTown)+"','"+str(applicantCounty)+"','"+str(applicantCountry)+"','"+str(applicantPostcode)+"','"+str(applicantTelephone)+"','"+str(applicantMobile)+"','"+str(applicantFax)+"','"+str(applicantEmail)+"','"+str(agentTitle)+"','"+str(agentFirstname)+"','"+str(agentSurname)+"','"+str(agentCompanyname)+"','"+str(agentStreetAddress)+"','"+str(agentTown)+"','"+str(agentCounty)+"','"+str(agentCountry)+"','"+str(agentPostcode)+"','"+str(agentTelephone)+"','"+str(agentMobile)+"','"+str(agentFax)+"','"+str(agentEmail)+"','"+str(proposal)+"','"+str(existingWallMaterial)+"','"+str(proposedWallMaterial)+"','"+str(existingRoofMaterial)+"','"+str(proposedRoofMaterial)+"','"+str(existingwindowMaterial)+"','"+str(proposedwindowMaterial)+"','"+str(existingDoorMaterial)+"','"+str(proposedDoorMaterial)+"','"+str(existingBoundaryTreatmentMaterial)+"','"+str(proposedBoundaryTreatmentMaterial)+"','"+str(existingVehicleaccessMaterial)+"','"+str(proposedVehicleaccessMaterial)+"','"+str(existingLightingMaterial)+"','"+str(proposedLightingMaterial)+"','"+str(existingChimneyMaterial)+"','"+str(proposedChimneyMaterial)+"','"+str(existingRainwaterGoodsMaterial)+"','"+str(proposedRainwaterGoodsMaterial)+"','"+str(existingCeilingMaterial)+"','"+str(proposedCeilingMaterial)+"','"+str(existingFloorMaterial)+"','"+str(proposedFloorMaterial)+"','"+str(existingotherMaterial)+"','"+str(proposedotherMaterial)+"','"+str(vehicleCar)+"','"+str(vehicleLightGood)+"','"+str(vehicleMotorcycle)+"','"+str(vehicleDisability)+"','"+str(vehicleCycle)+"','"+str(vehicleOther)+"','"+str(vehicleOtherDesc)+"','"+str(existingUse)+"','"+str(proposedHouses)+"','"+str(proposedFlatsMaisonettes)+"','"+str(proposedLiveWorkUnits)+"','"+str(proposedClusterFlats)+"','"+str(proposedShelteredHousing)+"','"+str(proposedBedsitStudios)+"','"+str(proposedUnknown)+"','"+str(proposedMarketHousingTotal)+"','"+str(existingHouses)+"','"+str(existingFlatsMaisonettes)+"','"+str(existingLiveWorkUnits)+"','"+str(existingClusterFlats)+"','"+str(existingShelteredHousing)+"','"+str(existingBedsitStudios)+"','"+str(existingUnknown)+"','"+str(existingMarketHousingTotal)+"','"+str(totalProposedResidentialUnits)+"','"+str(totalExistingResidentialUnits)+"','"+str(nonResidentialShops)+"','"+str(nonResidentialFinancial)+"','"+str(nonResidentialRestaurants)+"','"+str(nonResidentialDrinking)+"','"+str(nonResidentialFood)+"','"+str(nonResidentialOffice)+"','"+str(nonResidentialResearch)+"','"+str(nonResidentialLight)+"','"+str(nonResidentialGeneral)+"','"+str(nonResidentialStorage)+"','"+str(nonResidentialHotels)+"','"+str(nonResidentialResidential)+"','"+str(nonResidentialInstitutions)+"','"+str(nonResidentialAssembly)+"','"+str(nonResidentialOther)+"','"+str(nonResidentialTotal)+"','"+str(existingEmployee)+"','"+str(proposedEmployee)+"','"+str(siteArea)+"','"+Material_Additional_Information+"','"+Material_Additional_Info_Details+"','"+Existing_Residential_House
	return buildInsertQuery
