# python 2.7

import sys
import re
import os
import imp,math
# import importlib
# import math

basePath = os.getcwd()
rootPath=os.path.basename(basePath)
basePath=basePath.replace(rootPath,'')
iniFilePath=basePath+'ctrl'
dataFilePath=basePath+'data'
libraryFilePath=basePath+'lib'
logFilePath=basePath+'logs'


print ("libraryFilePath::",libraryFilePath)


# name,path,dec=imp.find_module('oneAppDBTransactions', libraryFilePath)
# print ("name::",name)

oneAppDBTransactions = imp.load_source('oneAppDBTransactions', libraryFilePath+'\\oneAppDBTransactions.py')
oneAppUtility = imp.load_source('OneappUtility', libraryFilePath+'\\OneappUtility.py')
oneAppScraper = imp.load_source('oneAppScraper', libraryFilePath+'\\oneAppScraper.py')

print ("oneAppDBTransactions::",oneAppDBTransactions)

Core=oneAppUtility.readINI(iniFilePath,"core.ini")
regexFile=oneAppUtility.readINI(iniFilePath,"regex.ini")
pdfLocation=Core.get('pdfLocation', 'location') # read pdf stored path
selectQuery=Core.get('selectInput', 'selectQuery') # read select query
insertQuery=Core.get('insertQuery', 'insertQuery') # read insert query template

dbh=oneAppDBTransactions.dbConnection(Core,logFilePath)
inputList=oneAppDBTransactions.retriveInput(dbh,selectQuery,logFilePath)

print ("inputList::",inputList)

imageFormatList=[]

otherFormatList=[]
oneAppFormatList=[]

for i in range(len(inputList)):
	Format_ID 	= str(inputList[i][0])
	Council_Code 	= str(inputList[i][1])
	Document_Name 	= str(inputList[i][2])
	Application_No 	= str(inputList[i][3])
	Source 		= str(inputList[i][4])
	Markup 		= str(inputList[i][5])
	print ("Format_ID:",Format_ID)

	pdfFileNameWithPath = pdfLocation+'/'+Format_ID+'/'+Document_Name
	textFileNameWithPath = pdfFileNameWithPath;
	textFileNameWithPath = textFileNameWithPath.replace('.pdf','.txt')
	print ("textFileNameWithPath::",textFileNameWithPath)
	pdfFilecontent=''
	try:
		pdfFilecontent = oneAppUtility.convertPDF(libraryFilePath,pdfFileNameWithPath,textFileNameWithPath)
	except Exception as e:	
		print ("Error:",e)

	convertedTxtLength = len(pdfFilecontent)
	print ("Length :",convertedTxtLength)

	insertQueryTmp=''
	if convertedTxtLength != 0:
		(pdfFormat, pdfFilecontent, pdfFormatTmp) = oneAppUtility.formatIdentify(pdfFilecontent)
		print ("pdfFormat:",pdfFormat)
		if pdfFormat != 'Others':
			insertQueryTmp = oneAppScraper.scrapePDF(pdfFileNameWithPath,textFileNameWithPath,pdfFormat,pdfFilecontent,regexFile,oneAppUtility)
			insertQuery = insertQuery+"("+"'"+str(Source)+"','"+str(Format_ID)+"','"+str(Application_No)+"','"+str(Council_Code)+"','"+str(Markup)+"','"+str(insertQueryTmp)+"','"+str(pdfFormatTmp)+"');"
			print ("insertQuery:",insertQuery)
			if re.search('values\s*$',insertQuery) is None:
				# oneAppDBTransactions.queryExecution(dbh, insertQuery, logFilePath)
				oneAppFormatList.append(Format_ID)
		else:
			otherFormatList.append(Format_ID)
	else:
		imageFormatList.append(Format_ID)

# if oneAppFormatList:
    # oneAppDBTransactions.updateFormat(dbh, oneAppFormatList,"Y",logFilePath)

# if imageFormatList:
    # oneAppDBTransactions.updateFormat(dbh, imageFormatList,"I",logFilePath)

# if otherFormatList:
    # oneAppDBTransactions.updateFormat(dbh, otherFormatList,"O",logFilePath)
