import xlrd
import sys
import re
import os
import imp,math
import codecs

print "Dev Test

basePath = os.getcwd()
rootPath=os.path.basename(basePath)
basePath=basePath.replace(rootPath,'')
iniFilePath=basePath+'ctrl'
dataFilePath=basePath+'data'
libraryFilePath=basePath+'lib'
logFilePath=basePath+'logs'

# print ("libraryFilePath::",libraryFilePath)

oneAppDBTransactions = imp.load_source('oneAppDBTransactions', libraryFilePath+'\\oneAppDBTransactions.py')
oneAppUtility = imp.load_source('OneappUtility', libraryFilePath+'\\OneappUtility.py')
oneAppScraper = imp.load_source('oneAppScraper', libraryFilePath+'\\oneAppScraper.py')

# print ("oneAppDBTransactions::",oneAppDBTransactions)

Core=oneAppUtility.readINI(iniFilePath,"core.ini")
regexFile=oneAppUtility.readINI(iniFilePath,"regex.ini")
pdfLocation=Core.get('pdfLocation', 'location') # read pdf stored path
selectQuery=Core.get('selectInput', 'selectQuery') # read select query
insertQuery=Core.get('insertQuery', 'insertQuery') # read insert query template
Headers=Core.get('Headers', 'Headers') # read Headers template

# dbh=oneAppDBTransactions.dbConnection(Core,logFilePath)
# inputList=oneAppDBTransactions.retriveInput(dbh,selectQuery,logFilePath)

imageFormatList=[]

otherFormatList=[]
oneAppFormatList=[]
  
# loc = ("C:\\Users\\prabhud\\Documents\\Live_Project\\2020\\OneApp_new\\Data_Point_Test\\One_App\\root\\Format1.xls")

with open("Pdf_Format_Status.txt","w") as f:
	f.write("Format_ID"+"\t"+"Council_Code"+"\t"+"Document_Name"+"\t"+"pdfFormat"+"\n")

Headers=Headers.replace(",","\t")
# print ("Headers::",Headers)
# raw_input("***")

with open("Output.txt","w") as f:
	f.write(Headers+"\n")	
  
# wb = xlrd.open_workbook(loc) 
wb = xlrd.open_workbook("Input.xls") 
sheet = wb.sheet_by_index(0) 

inputList=sheet.cell_value

for i in range(sheet.nrows):
	Format_ID=str(sheet.cell_value(i, 0))
	Council_Code=str(sheet.cell_value(i, 1))
	Document_Name=str(sheet.cell_value(i, 2))
	Application_No=str(sheet.cell_value(i, 3))
	Source = str(sheet.cell_value(i, 4))
	Markup = str(sheet.cell_value(i, 5))
	print ("Format_ID:",Format_ID)
	# print ("Council_Code:",Council_Code)
	# print ("Document_Name:",Document_Name)

	
	pdfFileNameWithPath = pdfLocation+'/'+Format_ID+'/'+Document_Name
	textFileNameWithPath = pdfFileNameWithPath;
	textFileNameWithPath = textFileNameWithPath.replace('.pdf','.txt')
	# print ("textFileNameWithPath::",textFileNameWithPath)
	
	# raw_input("Path")
	
	pdfFilecontent=''
	try:
		pdfFilecontent = oneAppUtility.convertPDF(libraryFilePath,pdfFileNameWithPath,textFileNameWithPath)
	except Exception as e:	
		print ("Error:",e)

	convertedTxtLength = len(pdfFilecontent)
	# print ("Length :",convertedTxtLength)
	# raw_input("PDF_COnverted")
	insertQueryTmp=''
	if convertedTxtLength != 0:
		(pdfFormat, pdfFilecontent, pdfFormatTmp) = oneAppUtility.formatIdentify(pdfFilecontent)
		with open("Reconstructed_File.txt","w") as f:
				f.write(str(pdfFilecontent))
				
		with open("Pdf_Format_Status.txt","a") as f:
				f.write(str(Format_ID)+"\t"+str(Council_Code)+"\t"+str(Document_Name)+"\t"+str(pdfFormat)+"\n")
		# print ("pdfFormatTmp:",pdfFormatTmp)
		# raw_input("pdfFormat")
		
		if pdfFormat != 'Others':
			insertQueryTmp = oneAppScraper.scrapePDF(pdfFileNameWithPath,textFileNameWithPath,pdfFormat,pdfFilecontent,regexFile,oneAppUtility)
			
			insertQuery = insertQuery+"("+"'"+str(Source)+"','"+str(Format_ID)+"','"+str(Application_No)+"','"+str(Council_Code)+"','"+str(Markup)+"','"+str(insertQueryTmp)+"','"+str(pdfFormatTmp)+"');"
			
			Output = str(Source)+"','"+str(Format_ID)+"','"+str(Application_No)+"','"+str(Council_Code)+"','"+str(Markup)+"','"+str(insertQueryTmp)+"','"+str(pdfFormatTmp)
			
			Output=Output.replace("','","\t")
			
			# print ("insertQuery:",insertQuery)
			
			with open("Output.txt","a") as f:
				f.write(Output+"\n")
			if re.search('values\s*$',insertQuery) is None:
				## oneAppDBTransactions.queryExecution(dbh, insertQuery, logFilePath)
				oneAppFormatList.append(Format_ID)
		else:
			otherFormatList.append(Format_ID)
	else:
		imageFormatList.append(Format_ID)

## if oneAppFormatList:
	## oneAppDBTransactions.updateFormat(dbh, oneAppFormatList,"Y",logFilePath)

## if imageFormatList:
	## oneAppDBTransactions.updateFormat(dbh, imageFormatList,"I",logFilePath)

## if otherFormatList:
	## oneAppDBTransactions.updateFormat(dbh, otherFormatList,"O",logFilePath)